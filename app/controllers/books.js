import Controller from '@ember/controller';

export default Controller.extend({
      actions: {
    filterByName(param) {
      if (param !== '') {
        return this.get('store').query('book', { name: param });
      } else {
        return this.get('store').findAll('book');
      }
    }
  }
});

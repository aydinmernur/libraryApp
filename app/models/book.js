import DS from 'ember-data';

export default DS.Model.extend({
  author: DS.attr(),
  name: DS.attr(),
  image: DS.attr(),
});

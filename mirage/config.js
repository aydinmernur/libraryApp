export default function() {
    this.namespace = '/api';

    let books = [{
	type: 'books',
	id: 'the-complete-malazan-book-of the-fallen',
	attributes: {
	    author: 'Steven Erikson',
	    name: 'The Complete Malazan Book Of The Fallen',
	    image: '../assets/images/book1.jpg',
	}
    }, {
	type: 'books',
	id: 'ordinary-grace',
	attributes: {
	    author: 'William Kent Kreuger',
	    name: 'Ordinary Grace',
	    image: '../assets/images/book2.jpg',
	}
    }, {
	type: 'books',
	id: 'white-rose',
	attributes: {
	    author: 'Glen Cook',
	    name: 'White Rose',
	    image: '../assets/images/book3.jpg',
	}
    }];

    this.get('/books', function(db, request) {
	if(request.queryParams.name !== undefined) {
	    let filteredRentals = books.filter(function(i) {
		return i.attributes.name.toLowerCase().indexOf(request.queryParams.name.toLowerCase()) !== -1;
	    });
	    return { data: filteredRentals };
	} else {
	    return { data: books };
	}
    });
}


